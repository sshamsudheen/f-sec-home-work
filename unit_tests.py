from unittest import TestCase, mock
from arraysplitlibrary import LibArraySplit
from nose.tools import *
import os


class arraySplitLibTest(TestCase):

    def setUp(self) -> None:
        self.valid_items = ['foo_str1', 'foo_str2']
        self.invalid_item = 'foo_data'
        self.max_size_of_record = os.environ.get('max_size_of_record') 
        self.max_batch_count = os.environ.get('max_batch_count')
        return super().setUp()

    def test_array_split_with_invalid_list(self):
        with self.assertLogs() as captured:
            batches = LibArraySplit(self.invalid_item)
            filtered_array = batches.remove_large_record_set()
            batch_records = batches.split_in_to_bacthes()        
        err_msg = f'invalid input for item list, {self.invalid_item} is not an array'
        assert_equal(captured.records[0].message, err_msg);

    @mock.patch.dict(os.environ, {'max_size_of_record': 'foo_invalid'}, clear=True)
    def test_array_split_with_invalid_max_record_size(self):
        with self.assertLogs() as captured:
            batches = LibArraySplit(self.valid_items)
            filtered_array = batches.remove_large_record_set()
            batch_records = batches.split_in_to_bacthes()
        err_msg = f'invalid input for max_size_of_record, {os.environ.get("max_size_of_record")} is not int'
        assert_equal(captured.records[0].message, err_msg);
    
    @mock.patch.dict(os.environ, {'max_batch_count': 'foo_invalid_batch_count'}, clear=True)
    def test_array_split_with_invalid_max_batch_count(self):
        with self.assertLogs() as captured:
            batches = LibArraySplit(self.valid_items)
            filtered_array = batches.remove_large_record_set()
            batch_records = batches.split_in_to_bacthes()
        err_msg = f'invalid input for max_batch_count, {os.environ.get("max_batch_count")} is not int'
        assert_equal(captured.records[0].message, err_msg);
    
    @mock.patch.dict(os.environ, {'max_size_of_batch': 'foo_invalid_batch_count'}, clear=True)
    def test_array_split_with_invalid_max_batch_size(self):
        with self.assertLogs() as captured:
            batches = LibArraySplit(self.valid_items)
            filtered_array = batches.remove_large_record_set()
            batch_records = batches.split_in_to_bacthes()
        err_msg = f'invalid input for max_size_of_batch, {os.environ.get("max_size_of_batch")} is not int'
        assert_equal(captured.records[0].message, err_msg);

    # @mock.patch.dict(os.environ, {'max_size_of_record': 500}, clear=True)
    def test_array_split_happy(self):
        batches = LibArraySplit(self.valid_items)
        filtered_array = batches.remove_large_record_set()
        batch_records = batches.split_in_to_bacthes()
        assert_equal(len(list(batch_records)), 1);