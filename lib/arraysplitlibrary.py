import logging
import os
from typing import Generator
log_format = '%(asctime)s %(levelname)s %(message)s'
log_file_name = 'homework.log'
logging.basicConfig(format=log_format, filename=log_file_name, encoding='utf-8', level=logging.DEBUG)


class LibArraySplit:

    filtered_array = [str]

    def __init__(self, items):
        self.items = items
        # Configuration variables, either it can be fetched from environment variable ot have default values
        self.max_size_of_record = os.environ.get('max_size_of_record', 1024)  # set the max record size in bytes 1kb for ex
        self.max_size_of_batch = os.environ.get('max_size_of_batch', 10240)  # set the max record size in bytes, ex: 1mb
        self.max_batch_count = os.environ.get('max_batch_count', 50)  # set the maximum number iof records in batches
        self.get_size = len

    def split_in_to_bacthes(self) -> Generator:
        """
        SPLIT the given records to batches
        INPUT PARAMS:
            items: list/array of item as input
            max_size_of_record: mazimum zise of the output batch
            max_batch_count: maximum number of records in a bacth
        OUTPUT:
            List of bacthes as generator
        """
        buffer = []
        # Initialize buffer size as 0
        buffer_size = 0
        item_count = 0
        for item in self.filtered_array:
            # get the size of the item
            item_size = self.get_size(item)
            # buffer_size should not exceed max_size and item_count should not exceed max_batch_count
            if (buffer_size + item_size <= self.max_size_of_batch) and (item_count < self.max_batch_count):
                logging.debug(f'adding the record {item} to batch ')
                buffer.append(item)
                buffer_size += item_size
            else:
                yield buffer
                buffer = [item]
                buffer_size = item_size
                item_count = 0
            item_count += 1
        if buffer_size > 0:
            yield buffer

    def remove_large_record_set(self) -> bool:
        """
        REMOVE THE LARGE RECORD SET WHICH ARE GREATER THAN SIZE max_record_size
        INPUT PARAMS:
            record_list: list of records
            max_record_size: int in bytes
        OUTPUT :
            list of records whcih are less than max_record_size
        """
        logging.debug(f'Find and remove the record set which is more than {self.max_size_of_record} bytes')
        is_valid_input = self.validate_input()
        if not is_valid_input:
            return False
        record_list = []
        for arr in self.items:
            size = self.get_size(arr)

            if size > self.max_size_of_record:
                logging.warning(f'removed the record {arr} with record size {size} bytes')
                record_list = list(filter(lambda x: x != arr, self.items))
        if record_list:
            self.filtered_array = record_list
        else:
            self.filtered_array = self.items

        return True

    def validate_input(self):
        is_valid = True
        if type(self.items) is not list:
            logging.error(f'invalid input for item list, {self.items} is not an array')
            is_valid = False
        if type(self.max_size_of_record) is not int:
            logging.error(f'invalid input for max_size_of_record, {self.max_size_of_record} is not int')
            is_valid = False
        if type(self.max_batch_count) is not int:
            logging.error(f'invalid input for max_batch_count, {self.max_batch_count} is not int')
            is_valid = False
        if type(self.max_size_of_batch) is not int:
            logging.error(f'invalid input for max_size_of_batch, {self.max_size_of_batch} is not int')
            is_valid = False

        return is_valid

# class ended
