# F Sec Home Work



## Requirements

* Python3
* flake8
* nose2
* mypy

## Library
The library to split the array in to baches depends up on max batch count / max batch size and max record size, The library is placed in lib directory named **arraysplitlibrary.py**

File **array_split.py** use the above library and split the given records in to bacthes

```

python3 array_split.py

```

basic unit tests are written in nose2 , to install the necessary libs/packages run 

```

pip3 install -r requirements.txt

```
this will install nose2 , flake8, mypy

some basic unit tests are added in **unit_test.py** which can be testted using the below command

```

nose2 unit_tests

```

this also follows some standars like flake8 and mypy validattion, to check these use the below command

```

flake8 --config .flake8 <filename>
flake8 --config .flake8 lib/arraysplitlibrary.py

mypy --ignore-missing-imports <pythonfilename>
mypy --ignore-missing-imports array_split.py

```

Logger is also enabled in this project, it will create file **homework.log** under project directory and store the logs, this file is now added to .gitignore
